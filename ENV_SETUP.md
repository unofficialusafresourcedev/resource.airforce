### Environment Setup
Many contributors may not be familiar with Python and Flask development.
Below is an easy way to get your development environment up and running to contribute.
Feel free to email [unofficialusafresourcedev@gmail.com](mailto:unofficialusafresourcedev@gmail.com) for any questions or clarification.


Ubuntu (Install necessary software):
`$ sudo apt install python3-virtualenv`

Ubuntu (Configure environment):
```
$ mkdir resource.airforce
$ cd resource.airforce
$ virtualenv -p /usr/bin/python3 .
$ source bin/activate
$ git clone 'https://bitbucket.org/unofficialusafresourcedev/resource.airforce.git'
$ cd resource.airforce
$ pip3 install -r requirements.txt
$ python resource_airforce.py
```

Ubuntu (Subsequent runs):
```
$ cd resource.airforce
$ source bin/activate
$ cd resource.airforce
$ python resource_airforce.py
```
