**This site is neither created nor endorsed by the United States Air Force.**

# resource.airforce

A set of independent tools for helping Airmen calculate common information. (WAPS/BTZ/SRB...)

## About the Project

### Why?
Prior to this site, the best source for Airmen-oriented calculator-utilities were Excel spreadsheets shared online.
This is extremely risky, with plenty of points where malware could be introduced.
Considering that [1/4 of malware in Q3 2017](https://securelist.com/it-threat-evolution-q3-2017-statistics/83131/)
was introduced via Office files, this is a serious security concern.

### How is this better?
The utilities presented here are not downloaded and do not require any scripting (feel free to disable Javascript).
This means there is very low risk for malware transmission.

## Contributing

### Repository Access
There is a [public repository](https://bitbucket.org/unofficialusafresourcedev/resource.airforce) and a private repo.
The public repo has commit information stripped before being updated on Bitbucket.
This is an opsec measure.

For access to the private repo, please send an email to
[unofficialusafresourcedev@gmail.com](mailto:unofficialusafresourcedev@gmail.com).
Your military affiliation will be verified prior to access.

Feel free to submit a pull request if you are not military-affilited,
but you will not be given access to the private repo.

### Environment Setup
For assistance setting up your development environment, please view ENV_SETUP in the repository.
