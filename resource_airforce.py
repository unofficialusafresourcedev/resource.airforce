import csv
import datetime
import decimal
from dateutil.relativedelta import *
import markdown
import pandas as pd
import re
from flask import Flask, Markup, flash, make_response, render_template, request
from wtforms import Form, IntegerField, DecimalField, SelectField, StringField, validators, ValidationError, \
	BooleanField
from wtforms.ext.dateutil.fields import DateField

app = Flask(__name__)
app.config.update(
	SECRET_KEY='None',
	WTF_CSRF_ENABLED=False,
)

# TODO: Unit Tests
# TODO: Split modules into folders

mil_date_format = u'%d %b %y'
csp_policy = u"default-src 'none'; img-src 'self'; style-src 'self';"
msg_form_error = u'The form contained errors.'
modules = list()


class ModuleInfo:
	updated = None
	references = None

	def __init__(self, updated, *references):
		self.updated = datetime.datetime.strptime(updated, '%d %b %y %H:%M')
		self.references = references


epr_selections = [(250, u'Promote Now'),
				  (220, u'Must Promote'),
				  (200, u'Promote'),
				  (150, u'Not Ready Now'),
				  (50, u'Do Not Promote')]

# ===== NCO WAPS =====
modules.append(('/nco_waps/', 'NCO WAPS Calculator'))
nco_waps_info = ModuleInfo('01 Feb 18 15:00',
								'http://www.afpc.af.mil/Portals/70/documents/Life%20and%20Career/'
								'Promotions/AFI%2036-2502.pdf?ver=2017-01-11-154024-243',
								'Table 2.2, Page 38. TIG & TIS ignored per MyPers beginning in the 17 cycle.',
								'https://mypers.af.mil/ci/fattach/get/6269179/1480521983/redirect/1/filename/WAPS'
								'%20Eligibility%20Chart%20(Nov%2016).pdf',
								'https://mypers.af.mil/app/answers/detail/a_id/13016/p/10/c/656')


class NCOWAPSForm(Form):
	afscs = list()
	afsc_file = pd.read_csv('reference_data/afsc_list.csv')
	for row in afsc_file.iterrows():
		afscs.append((row[1]['AFSC'], row[1]['AFSC']))
	afscs.sort()

	empty_field = list([(-1, u'-----')])
	eligibility = StringField(u'Eligibility To Test')
	rank_testing_for = SelectField(u'Rank Testing For', choices=[(u'e5', u'SSgt'), (u'e6', u'TSgt')],
								   validators=[validators.DataRequired()])
	cycle_testing_in = IntegerField(u'Cycle Testing In', validators=[validators.DataRequired()],
									render_kw={u"placeholder": datetime.datetime.now().year})
	initial_enlistment_date = DateField(u'Initial Enlistment Date', validators=[validators.DataRequired()],
										render_kw={u"placeholder": '05 Jan 18'})
	date_of_rank = DateField(u'Date of Rank', validators=[validators.DataRequired()],
							 render_kw={u"placeholder": '05 Jan 18'})
	estimated_cutoff = DecimalField(u'Estimated Cutoff Score', validators=[validators.DataRequired()], places=2)
	decorations = IntegerField(u'Decoration Points', validators=[validators.NumberRange(0, 25)])
	pfe = DecimalField(u'Promotion Fitness Exam (PFE) Score',
					   validators=[validators.InputRequired(), validators.NumberRange(0, 100)], places=2)
	skt = DecimalField(u'Specialty Knowledge Test (SKT) Score', validators=[validators.NumberRange(0, 100)],
					   places=2)
	afsc = SelectField(u'AFSC', choices=afscs, validators=[validators.DataRequired()])
	epr1 = SelectField(u'Newest EPR', choices=epr_selections, validators=[validators.DataRequired()],
					   coerce=int, default=200)
	epr2 = SelectField(u'EPR 2', choices=empty_field + epr_selections, default=-1, coerce=int)
	epr3 = SelectField(u'EPR 3', choices=empty_field + epr_selections, default=-1, coerce=int)
	total_score = DecimalField(u'Total WAPS Score', places=2)
	is_promoted = StringField(u'Result based upon scores:')

	def reset_form_results(self):
		self.eligibility.data = ''
		self.total_score.data = decimal.Decimal()

	def calculate(self):
		# Promotion Scores
		skt = self.skt.data
		pfe = self.pfe.data
		decorations = int(self.decorations.data)
		# epr1 always filled
		epr1 = self.epr1.data
		# Pre-set epr_score in case none of the other fields are filled
		# epr1 counts for 100% if it's the only epr
		epr_score = epr1
		if self.epr2.data != -1:
			epr2 = self.epr2.data
			if self.epr3.data != -1:
				epr3 = self.epr3.data
				# All 3 filled, process as 1 - 50%; 2 - 30%; 3 - 20%;
				epr_score = (epr1 * .50) + (epr2 * .30) + (epr3 * .20)
			else:
				# Only two filled, process as 1 - 60%; 2 - 40%;
				epr_score = (epr1 * .60) + (epr2 * .40)
		total_score = skt + pfe + decorations + decimal.Decimal(epr_score)
		self.total_score.data = total_score

		# Promotion Eligibility
		date_of_rank = self.date_of_rank.data
		initial_enlistment_date = self.initial_enlistment_date.data
		if self.rank_testing_for.data == 'e5':
			# if date of rank is 6 months prior to 1 feb of test year
			if (date_of_rank + relativedelta(months=6)) < datetime.date(self.cycle_testing_in.data, 2, 1):
				# if initial enlistment date is 3 years prior to 1 aug of test year
				if (initial_enlistment_date + relativedelta(years=3)) < datetime.date(self.cycle_testing_in.data, 8, 1):
					self.eligibility.data = 'Eligible'
				else:
					self.eligibility.data = 'Ineligible - TIS'
			else:
				self.eligibility.data = 'Ineligible - TIG'
		else:  # e6
			# if date of rank is 23 months prior to 1 aug of test year
			print(date_of_rank + relativedelta(months=23))
			print(datetime.date(self.cycle_testing_in.data, 8, 1))
			if (date_of_rank + relativedelta(months=23)) < datetime.date(self.cycle_testing_in.data, 8, 1):
				# if initial enlistment date is 5 years prior to 1 jul of test year
				if (initial_enlistment_date + relativedelta(years=5)) < datetime.date(self.cycle_testing_in.data, 7, 1):
					self.eligibility.data = 'Eligible'
				else:
					self.eligibility.data = 'Ineligible - TIS'
			else:
				self.eligibility.data = 'Ineligible - TIG'

		if total_score > self.estimated_cutoff.data:
			self.is_promoted.data = 'Promoted 😃'
		else:
			self.is_promoted.data = 'Not Promoted 😢'


@app.route('/nco_waps/', methods=['GET', 'POST'])
def nco_waps():
	form = NCOWAPSForm(request.form)
	form.reset_form_results()
	if request.method == 'POST' and not form.validate():
		flash(msg_form_error, u'danger')
	if request.method == 'POST' and form.validate():
		form.calculate()
	resp = make_response(render_template('nco_waps.html', form=form, info=nco_waps_info))
	resp.headers.set(u'Content-Security-Policy', csp_policy)
	return resp


# ===== BTZ =====
modules.append(('/btz/', 'SrA BTZ Calculator'))
btz_info = ModuleInfo('02 Feb 18 14:00',
					  'http://www.afpc.af.mil/Promotion/Senior-Airman-Below-The-Zone-Program/')


class BTZForm(Form):
	tafmsd = DateField(u'Total Active Federal Military Service Date (TAFMSD)',
					   validators=[validators.DataRequired()],
					   render_kw={u'placeholder': '05 Jan 18'})
	a1c_dor = DateField(u'A1C Date Of Rank (DOR)',
						validators=[validators.DataRequired()],
						render_kw={u'placeholder': '05 Jan 18'})
	enlist_duration = SelectField(u'Length of Enlistment',
								  choices=[(u'4yr', u'Four-Year'),
										   (u'6yr', u'Six-Year')],
								  validators=[validators.DataRequired()])
	enlist_rank = SelectField(u'Enlistment Rank',
							  choices=[(u'ab', u'Airman Basic'),
									   (u'amn', u'Airman'),
									   (u'a1c', u'Airman First Class')],
							  validators=[validators.DataRequired()])
	btz_promotion_date = DateField(u'BTZ-Selected Promotion Date', display_format=mil_date_format)
	sra_promotion_date = DateField(u'Regular Promotion Date', display_format=mil_date_format)
	meet_board_date = DateField(u'BTZ Board Date', display_format='%b%y')

	def reset_form_results(self):
		self.btz_promotion_date.data = ''
		self.sra_promotion_date.data = ''
		self.meet_board_date.data = ''

	def calculate(self):
		from dateutil.relativedelta import relativedelta
		# 6-Year Enlistee or A1C
		if self.enlist_duration.data == '6yr' or self.enlist_rank.data == 'a1c':
			self.sra_promotion_date.data = self.a1c_dor.data + relativedelta(months=+28)
			self.meet_board_date.data = (self.a1c_dor.data + relativedelta(months=+28)) + relativedelta(months=-7)
			self.btz_promotion_date.data = (self.a1c_dor.data + relativedelta(months=+28)) + relativedelta(months=-6)
		# 4-Year Enlistee
		elif self.enlist_duration.data == '4yr' and self.enlist_rank.data != 'a1c':
			compare_date = self.tafmsd.data + relativedelta(months=+36)
			if (self.a1c_dor.data + relativedelta(months=20)) > compare_date:
				compare_date = self.a1c_dor.data + relativedelta(months=20)
			self.sra_promotion_date.data = compare_date
			self.meet_board_date.data = compare_date + relativedelta(months=-7)
			self.btz_promotion_date.data = compare_date + relativedelta(months=-6)
		else:
			self.errors['calc_error'] = u'Unable to calculate results'


@app.route('/btz/', methods=['GET', 'POST'])
def btz():
	form = BTZForm(request.form)
	form.reset_form_results()
	if request.method == 'POST':
		if not form.validate():
			flash(msg_form_error, u'danger')
		else:
			form.calculate()
			for key in form.errors.keys():
				if key == 'calc_error':
					flash(form.errors['calc_error'], u'danger')
	resp = make_response(render_template('btz.html', form=form, info=btz_info))
	resp.headers.set(u'Content-Security-Policy', csp_policy)
	return resp


# ===== SNCO WAPS =====
# modules.append(('/snco/', 'SNCO Waps Calculator'))
sncowaps_info = ModuleInfo('06 Feb 18 08:38', 'https://temp.url')


# Custom WTFForm Validator
def score_check(name, minimum=-1, maximum=-1):
	message = '%s score must be between %d and %d.' % (name, minimum, maximum)

	def _score_check(form, field):
		if field.data < minimum or field.data > maximum:
			raise ValidationError(message)

	return _score_check


class SNCOWapsForm(Form):
	# Rank Testing For Field
	testing_rank = SelectField(u'Rank Testing For',
							   choices=[(u'msgt', u'Master Sergeant'),
										(u'smsgt', u'Senior Master Sergeant'),
										(u'cmsgt', u'Chief Master Sergeant')],
							   validators=[validators.DataRequired()])

	# Rank Testing For Field
	testing_rank = SelectField(u'Rank Testing For',
							   choices=[(u'msgt', u'Master Sergeant'),
										(u'smsgt', u'Senior Master Sergeant'),
										(u'cmsgt', u'Chief Master Sergeant')],
							   validators=[validators.DataRequired()])

	# Cycle Field
	cycle = SelectField(u'Cycle Testing in',
						choices=[(u'2018', u'2018'),
								 (u'2019', u'2019'),
								 (u'2020', u'2020')],
						validators=[validators.DataRequired()])

	# TAFMSD Field
	tafmsd = DateField(u'When did you join the Air Force?',
					   validators=[validators.DataRequired()],
					   render_kw={u'placeholder': datetime.datetime.now().strftime(mil_date_format)})

	# AFSC Field
	afsc = list()

	# load afscs in from csv
	afsc_list = pd.read_csv('reference_data/afsc_list.csv')

	# populate AFSC list with data from csv
	for index, row in afsc_list.iterrows():
		afsc.append(row[0])

	afsc.sort()

	# Medal Score Field
	medal_score = IntegerField(u'Medal Score',
							   validators=[validators.DataRequired(), score_check(name="Medal", minimum=0, maximum=25)],
							   render_kw={u'placeholder': '0'})

	# Board Scores Field
	board1 = DecimalField(u'Board Score 1',
						  validators=[validators.DataRequired(), score_check(name="Board", minimum=6, maximum=10)],
						  render_kw={u'placeholder': '0'})

	board2 = DecimalField(u'Board Score 2',
						  validators=[validators.DataRequired(), score_check(name="Board", minimum=6, maximum=10)],
						  render_kw={u'placeholder': '0'})

	board3 = DecimalField(u'Board Score 3',
						  validators=[validators.DataRequired(), score_check(name="Board", minimum=6, maximum=10)],
						  render_kw={u'placeholder': '0'})


# board_total = (board1.data + board2.data + board3.data) * 15


# @app.route('/snco/', methods=['GET', 'POST'])
def snco():
	form = SNCOWapsForm(request.form)

	if request.method == 'POST':
		if not form.validate():
			flash(msg_form_error, u'danger')
		else:
			form.calculate()
			for key in form.errors.keys():
				if key == 'calc_error':
					flash(form.errors['calc_error'], u'danger')

	selected = str(request.form.get('choice'))
	state = {'choice': selected}

	resp = make_response(render_template('snco.html', form=form, afsc_list=form.afsc, state=state, info=sncowaps_info))
	resp.headers.set(u'Content-Security-Policy', csp_policy)
	return resp

	if request.method == 'POST':
		if not form.validate():
			flash(msg_form_error, u'danger')
		else:
			form.calculate()
			for key in form.errors.keys():
				if key == 'calc_error':
					flash(form.errors['calc_error'], u'danger')
	resp = make_response(render_template('snco.html', form=form, info=sncowaps_info))
	resp.headers.set(u'Content-Security-Policy', csp_policy)
	return resp


# ===== SRB =====
modules.append(('/srb/', 'SRB Calculator'))
srb_info = ModuleInfo('03 Feb 18 15:34',
					  'https://mypers.af.mil/app/answers/detail/a_id/13829/p/10/c/698')


class SRBForm(Form):
	# AFSC Dictionary
	# key: afsc, values: zone a, zone b, zone c, zone e
	afsc = {}

	# load srb data in from csv
	srb_list = pd.read_csv('reference_data/srb_list.csv')
	srb_df = srb_list.set_index("AFSC")

	# populate the AFSC dictionary with the data located in srb_list
	for index, row in srb_df.iterrows():
		afsc[index] = [row[0], row[1], row[2], row[3]]

	# Form Display

	# Years Served Field
	years_enlisted = IntegerField(u'Time in Service (in years)',
								  validators=[validators.DataRequired()],
								  render_kw={u'placeholder': '0'})

	afscs = list()
	for key in afsc:
		afscs.append((key, key))  # WTForms expects a tuple, so we'll just give it twice
	afscs.sort()

	# Rank Field
	rank = SelectField(u'Current Rank',
					   choices=[(u'E-4', u'Senior Airman'),
								(u'E-5', u'Staff Sergeant'),
								(u'E-6', u'Tech Sergeant'),
								(u'E-7', u'Master Sergeant'),
								(u'E-8', u'Senior Master Sergeant'),
								(u'E-9', u'Chief Master Sergeant')],
					   validators=[validators.DataRequired()],
					   default=u'E-4')

	# Enlistment Length Field
	reenlistment = SelectField(u'Reenlistment Length',
							   choices=[(u'3', u'3 Years'),
										(u'4', u'4 Years'),
										(u'5', u'5 Years'),
										(u'6', u'6 Years')],
							   validators=[validators.DataRequired()],
							   default=u'3')

	# AFSC Field
	my_afsc = SelectField(u'Air Force Specialty Code (AFSC)',
						  choices=afscs,
						  validators=[validators.DataRequired()],
						  default=u'1A1X1')

	estimate = BooleanField(u'Estimate Lump Sum Disbursements')

	bonus = IntegerField(u'Reenlistment Bonus')
	lump_sum = IntegerField(u'Initial Lump Sum')
	disbursement = StringField(u'Annual Disbursements')

	def calculate(self):
		my_afsc = self.my_afsc.data
		base_pay = self.get_base_pay(self.rank.data, self.years_enlisted.data)
		enlistment_length = int(self.reenlistment.data)

		years = self.years_enlisted.data
		zone = "undefined"
		if 1 <= years < 6:
			zone = "A"
		elif 6 <= years < 10:
			zone = "B"
		elif 10 <= years <= 14:
			zone = "C"
		elif 18 <= years <= 20:
			zone = "E"

		multiplier = 0

		if zone == "A":
			multiplier = self.afsc[my_afsc][0]
		elif zone == "B":
			multiplier = self.afsc[my_afsc][1]
		elif zone == "C":
			multiplier = self.afsc[my_afsc][2]
		elif zone == "E":
			multiplier = self.afsc[my_afsc][3]

		# Either their zone doesn't have an SRB or they aren't in a zone at all (IE: TIS 15 - 17 years or > 20 years!)
		if multiplier == 0:
			flash('Sorry, you are not eligible for a SRB.', u'danger')
			return

		bonus_pay = (base_pay * multiplier) * enlistment_length
		self.bonus.data = bonus_pay

		if self.estimate.data:

			portion = 1
			if zone == "A":
				portion = .5
			elif zone == "B":
				portion = .75

			prorated_bonus = (bonus_pay * portion)
			remainder = bonus_pay - prorated_bonus
			self.lump_sum.data = format(prorated_bonus, '.2f')
			if remainder != 0:
				self.disbursement.data = format(remainder / enlistment_length, '.2f')
			else:
				self.disbursement.data = "Zone %s does not qualify for disbursements." % zone

	def get_base_pay(self, grade, tis):
		paychart = pd.read_csv('reference_data/paychart.csv')
		df2 = paychart.set_index("Grade")
		years = "2 or less"
		# TODO: find a more elegant solution
		if tis < 2:
			years = "2 or less"
		elif 2 <= tis < 3:
			years = "Over 2"
		elif 3 <= tis < 4:
			years = "Over 3"
		elif 4 <= tis < 6:
			years = "Over 4"
		elif 6 <= tis < 8:
			years = "Over 6"
		elif 8 <= tis < 10:
			years = "Over 8"
		elif 10 <= tis < 12:
			years = "Over 10"
		elif 12 <= tis < 14:
			years = "Over 12"
		elif 14 <= tis < 16:
			years = "Over 14"
		elif 16 <= tis < 18:
			years = "Over 16"
		elif 18 <= tis < 20:
			years = "Over 18"
		elif 20 <= tis < 22:
			years = "Over 20"
		elif 22 <= tis < 24:
			years = "Over 22"
		elif 24 <= tis < 26:
			years = "Over 24"
		elif 26 <= tis < 28:
			years = "Over 26"
		elif 28 <= tis < 30:
			years = "Over 28"
		elif 30 <= tis < 32:
			years = "Over 30"
		elif 32 <= tis < 34:
			years = "Over 32"
		elif 34 <= tis < 36:
			years = "Over 34"
		elif 36 <= tis < 38:
			years = "Over 36"
		elif 38 <= tis < 40:
			years = "Over 38"
		elif tis >= 40:
			years = "Over 40"
		return float(df2.loc[grade, years])


@app.route('/srb/', methods=['GET', 'POST'])
def srb():
	form = SRBForm(request.form)

	if request.method == 'POST':
		if not form.validate():
			flash(msg_form_error, u'danger')
		else:
			form.calculate()
			for key in form.errors.keys():
				if key == 'calc_error':
					flash(form.errors['calc_error'], u'danger')

	resp = make_response(render_template('srb.html', form=form, info=srb_info))
	resp.headers.set(u'Content-Security-Policy', csp_policy)
	return resp


# ===== PT Calculator =====


modules.append(('/pt/', 'PT Calculator'))
pt_info = ModuleInfo('18 Feb 18 10:36',
					 'http://www.afpc.af.mil/Portals/70/documents/Home/AF%20Fitness%20Program/FITNESS%20CHARTS.pdf')


# saves time so we don't have to manually enter each value for run times -- instead just provide a dict structured with
# a unique key value (points awarded) and the start and end run that corresponds to that key
def populate_run_dict(point_dict: dict):
	for key, value in point_dict.items():
		val1 = value[0].split(":")
		val2 = value[1].split(":")

		# Arbitrary year, month, day, and hour (we really only care for the minute and second value)
		t1 = datetime.datetime(2018, 1, 1, 1, int(val1[0]), int(val1[1]))
		t2 = datetime.datetime(2018, 1, 1, 1, int(val2[0]), int(val2[1]))

		delta = t2 - t1

		for i in range(delta.seconds + 1):
			val = t1 + datetime.timedelta(seconds=i)
			formatted_time = val.strftime("%M:%S")
			if formatted_time != value[0] and formatted_time != value[1]:
				point_dict[key].append(formatted_time)


#  print(point_dict)


# WTForm Validator
def runtime_validator():
	message = 'Invalid Run Time format -- Correct format is MM:SS'
	valid = re.compile('[0-5][0-9][:][0-5][0-9]$')

	def _runtime_validator(form, field):
		if not valid.match(field.data):
			raise ValidationError(message)

	return _runtime_validator


class PTForm(Form):
	# male under 30 point values
	mu30_pushup_dict = {10.0: [67], 9.5: [62, 63, 64, 65, 66], 9.4: [61], 9.3: [60], 9.2: [59], 9.1: [58], 9: [57],
						8.9: [56], 8.8: [55, 54], 8.7: [53], 8.6: [52], 8.5: [51], 8.4: [50], 8.3: [49], 8.1: [48],
						8: [47], 7.8: [46], 7.7: [45], 7.5: [44], 7.3: [43], 7.2: [42], 7: [41], 6.8: [40], 6.5: [39],
						6.3: [38], 6: [37], 5.8: [36], 5.5: [35], 5.3: [34], 5: [33]
						}

	mu30_situp_dict = {10.0: [58], 9.5: [57, 56, 55], 9.4: [54], 9.2: [53], 9: [52], 8.8: [51], 8.7: [50], 8.5: [49],
					   8.3: [48], 8.0: [47], 7.5: [46], 7: [45], 6.5: [44], 6.3: [43], 6.0: [42]
					   }

	mu30_run_dict = {60.0: ['09:12', '09:12'], 59.7: ['09:13', '09:34'], 59.3: ['09:35', '09:45'],
					 58.9: ['09:46', '09:58'], 58.5: ['09:59', '10:10'], 57.9: ['10:11', '10:23'],
					 57.3: ['10:24', '10:37'], 56.6: ['10:38', '10:51'], 55.7: ['10:52', '11:06'],
					 54.8: ['11:07', '11:22'], 53.7: ['11:23', '11:38'], 52.4: ['11:39', '11:56'],
					 50.9: ['11:57', '12:14'], 49.2: ['12:15', '12:33'], 47.2: ['12:34', '12:53'],
					 44.9: ['12:54', '13:14'], 42.3: ['13:15', '13:36']
					 }
	populate_run_dict(mu30_run_dict)

	mu30_waist_dict = {20.0: [32.5, 33, 33.5, 34, 34.5, 35], 17.6: [35.5], 17: [36.0], 16.4: [36.5], 15.8: [37],
					   15.1: [37.5], 14.4: [38], 13.5: [38.5], 12.6: [39]
					   }

	# male under 40 point values
	mu40_pushup_dict = {10.0: [57], 9.5: [52, 53, 54, 55, 56], 9.4: [51], 9.3: [50], 9.2: [48, 49], 9.1: [47],
						9.0: [46], 8.9: [45], 8.8: [44], 8.7: [43], 8.6: [42], 8.5: [41], 8.3: [40], 8.0: [39],
						7.8: [38], 7.7: [37], 7.5: [36], 7.3: [35], 7.0: [34], 6.8: [33], 6.7: [32], 6.5: [31],
						6.0: [30], 5.5: [29], 5.3: [28], 5.0: [27]
						}

	mu40_situp_dict = {10.0: [54], 9.5: [51, 52, 53], 9.4: [50], 9.2: [49], 9.0: [48], 8.8: [47], 8.7: [46],
					   8.5: [45], 8.3: [44], 8.0: [43], 7.5: [42], 7.0: [41], 6.5: [40], 6.0: [39]
					   }

	mu40_run_dict = {60.0: ['9:34', '9:34'], 59.3: ['9:35', '9:58'], 58.6: ['9:59', '10:10'], 57.9: ['10:11', '10:23'],
					 57.3: ['10:24', '10:37'], 56.6: ['10:38', '10:51'], 55.7: ['10:52', '11:06'],
					 54.8: ['11:07', '11:22'], 53.7: ['11:23', '11:38'], 52.4: ['11:39', '11:56'],
					 50.9: ['11:57', '12:14'], 49.2: ['12:15', '12:33'], 47.2: ['12:34', '12:53'],
					 44.9: ['12:54', '13:14'], 42.3: ['13:15', '13:36'], 39.3: ['13:37', '14:00']
					 }
	populate_run_dict(mu40_run_dict)

	mu40_waist_dict = {20.0: [32.5, 33.0, 33.5, 34.0, 34.5, 35.0], 17.6: [35.5], 17.0: [36.0], 16.4: [36.5],
					   15.8: [37.0], 15.1: [37.5], 14.4: [38.0], 13.5: [38.5], 12.6: [39.0]
					   }

	# male under 50 point values
	mu50_pushup_dict = {}

	mu50_situp_dict = {}

	mu50_run_dict = {}

	mu50_waist_dict = {}

	# female under 30 point values
	fu30_pushup_dict = {10.0: [47], 9.5: [42, 43, 44, 45, 46], 9.4: [41], 9.3: [40], 9.2: [39], 9.1: [38], 9.0: [37],
						8.9: [36], 8.8: [35], 8.6: [34], 8.5: [33], 8.2: [30], 8.1: [29], 8.0: [28], 7.5: [27],
						7.3: [26], 7.2: [25], 7.0: [24], 6.5: [23], 6.3: [22], 6.0: [21], 5.8: [20], 5.5: [19],
						5.0: [18]
						}

	fu30_situp_dict = {10.0: [54], 9.5: [51, 52, 53], 9.4: [50], 9.0: [49], 8.9: [48], 8.8: [47], 8.6: [46], 8.5: [45],
					   8.0: [44], 7.8: [43], 7.5: [42], 7.0: [41], 6.8: [40], 6.5: [39], 6.0: [38]
					   }

	fu30_run_dict = {60.0: ['10:23', '10:23'], 59.9: ['10:24', '10:51'], 59.5: ['10:52', '11:06'],
					 59.2: ['11:07', '11:22'], 58.9: ['11:23', '11:38'], 58.6: ['11:39', '11:56'],
					 58.1: ['11:57', '12:14'], 57.6: ['12:15', '12:33'], 57.0: ['12:34', '12:53'],
					 56.2: ['12:54', '13:14'], 55.3: ['13:15', '13:36'], 54.2: ['13:37', '14:00'],
					 52.8: ['14:01', '14:25'], 51.2: ['14:26', '14:52'], 49.3: ['14:53', '15:20'],
					 46.9: ['15:21', '15:50'], 44.1: ['15:51', '16:22']
					 }
	populate_run_dict(fu30_run_dict)

	fu30_waist_dict = {20.0: [29.0, 29.5, 30.0, 30.5, 31.0, 31.5], 17.6: [32.0], 17.1: [32.5], 16.5: [33.0],
					   15.9: [33.5], 15.2: [34.0], 14.5: [34.5], 13.7: [35.0], 12.8: [35.5]
					   }

	# female under 40 point values
	fu40_pushup_dict = {10.0: [46], 9.5: [40, 41, 42, 43, 44, 45], 9.4: [39], 9.3: [38, 37], 9.2: [36], 9.1: [34, 35],
						9.0: [33], 8.9: [31, 32], 8.8: [30], 8.7: [29], 8.6: [27, 28], 8.5: [26], 8.3: [25], 8.2: [24],
						8.0: [23], 7.9: [22], 7.8: [21], 7.6: [20], 7.5: [19], 7.0: [18], 6.8: [17], 6.5: [16],
						6.0: [15], 5.0: [14]
						}

	fu40_situp_dict = {10.0: [45], 9.5: [42, 43, 44], 9.4: [41], 9.0: [40], 8.8: [39], 8.5: [38], 8.3: [37], 8.2: [36],
					   8.0: [35], 7.8: [34], 7.5: [33], 7.0: [32], 6.8: [31], 6.5: [30], 6.0: [29]
					   }

	fu40_run_dict = {60.0: ['10:51', '10:51'], 59.5: ['10:52', '11:22'], 59.0: ['11:23', '11:38'],
					 58.6: ['11:39', '11:56'], 58.1: ['11:57', '12:14'], 57.6: ['12:15', '12:33'],
					 57.0: ['12:34', '12:53'], 56.2: ['12:54', '13:14'], 55.3: ['13:15', '13:36'],
					 54.2: ['13:37', '14:00'], 52.8: ['14:01', '14:25'], 51.2: ['14:26', '14:52'],
					 49.3: ['14:53', '15:20'], 46.9: ['15:21', '15:50'], 44.1: ['15:51', '16:22'],
					 40.8: ['16:23', '16:57']
					 }
	populate_run_dict(fu40_run_dict)

	fu40_waist_dict = {20.0: [29.0, 29.5, 30.0, 30.5, 31.0, 31.5], 17.6: [32.0], 17.1: [32.5], 16.5: [33.0],
					   15.9: [33.5], 15.2: [34.0], 14.5: [34.5], 13.7: [35.0], 12.8: [35.5]
					   }

	# Form Display

	# Gender Field
	gender = SelectField(u'Gender',
						 choices=[(u'male', u'Male'),
								  (u'female', u'Female')],
						 validators=[validators.InputRequired()],
						 default=u'male')

	# Age Bracket Field
	age = SelectField(u'Age Bracket',
					  choices=[(u'u30', u'18 - 30'),
							   (u'u40', u'30 - 39'),
							   (u'u50', u'40 - 49'),
							   (u'u60', u'50 - 59'),
							   (u'u70', u'60 - ∞'), ],
					  validators=[validators.InputRequired()],
					  default=u'u30')

	# Pushup Field
	pushups = IntegerField(u'Pushups',
						   validators=[validators.InputRequired()],
						   render_kw={u'placeholder': '0'})

	# Situp Field
	situps = IntegerField(u'Situps',
						  validators=[validators.InputRequired()],
						  render_kw={u'placeholder': '0'})

	# Run Field
	run = StringField(u'Run Time',
					  validators=[validators.DataRequired(), runtime_validator()],
					  render_kw={u'placeholder': '09:54'})

	# Waist Field
	wst_values = list()
	for i in range(25, 51):
		wst_values.append((i, i))  # WTForms expects a tuple
		wst_values.append((i + .5, i + .5))

	wst = SelectField(u'Waist Measurement',
					  coerce=float,
					  choices=wst_values,
					  validators=[validators.DataRequired()],
					  default=u'25')

	# Result Fields

	composite_res = StringField(u'PT Test Score')

	run_res = StringField(u'Run Points')

	wst_res = StringField(u'Waist Points')

	pushup_res = StringField(u'Pushup Points')

	situp_res = StringField(u'Situp Points')

	# Exemption Checkboxes

	run_exempt = BooleanField(u'Run Exempt')

	wst_exempt = BooleanField(u'Waist Exempt')

	pushup_exempt = BooleanField(u'Pushup Exempt')

	situp_exempt = BooleanField(u'Situp Exempt')

	def get_composite_score(self, run: float, wst: float, psh: float, sit: float):
		perfect_score = 100
		my_score = 0
		if self.run_exempt.data:
			perfect_score -= 60
		else:
			my_score += run
		if self.wst_exempt.data:
			perfect_score -= 20
		else:
			my_score += wst
		if self.pushup_exempt.data:
			perfect_score -= 10
		else:
			my_score += psh
		if self.situp_exempt.data:
			perfect_score -= 10
		else:
			my_score += sit

		# Check for failed components and whether they were exempt or not
		failed_component = False
		if self.run_exempt.data:
			self.run_res.data = 'Exempt'
		else:
			if run == 0:
				self.run_res.data = 'Failed'
				failed_component = True
			else:
				self.run_res.data = "%.2f / %d" % (run, 60)

		if self.wst_exempt.data:
			self.wst_res.data = 'Exempt'
		else:
			if wst == 0:
				self.wst_res.data = 'Failed'
				failed_component = True
			else:
				self.wst_res.data = "%.2f / %d" % (wst, 20)

		if self.pushup_exempt.data:
			self.pushup_res.data = 'Exempt'
		else:
			if psh == 0:
				self.pushup_res.data = 'Failed'
				failed_component = True
			else:
				self.pushup_res.data = "%.2f / %d" % (psh, 10)

		if self.situp_exempt.data:
			self.situp_res.data = 'Exempt'
		else:
			if sit == 0:
				self.situp_res.data = 'Failed'
				failed_component = True
			else:
				self.situp_res.data = "%.2f / %d" % (sit, 10)

		# Calculate composite score
		if perfect_score == 0:
			self.composite_res.data = 'Why did you even test? ;)'
		else:
			result = (my_score / perfect_score) * 100
			if not failed_component and result > 75:
				self.composite_res.data = 'Passed: %.2f' % result
			else:
				self.composite_res.data = 'Failed: %.2f' % result

	def get_wst_points(self, point_dict: dict, size: float, fail: float, maximum: float):
		points = -1
		if size <= maximum:
			points = 20
		elif size >= fail:
			points = 0
		else:
			for key, value in point_dict.items():
				for val in value:
					if size == val:
						return key
		return points

	def get_str_points(self, point_dict: dict, total: int, fail: int, maximum: int):
		if total >= maximum:
			return 10
		elif total <= fail:
			return 0
		else:
			for key, value in point_dict.items():
				for val in value:
					if total == val:
						return key

	def get_run_points(self, point_dict: dict, time: str, fail: str, maximum: str):
		tmp_min = fail.split(":")
		tmp_max = maximum.split(":")
		tmp_time = time.split(":")

		points = -1

		if int(tmp_time[0]) < int(tmp_max[0]):
			points = 60
		elif int(tmp_time[0]) == int(tmp_max[0]):
			if int(tmp_time[1]) <= int(tmp_max[1]):
				points = 60
		elif int(tmp_time[0]) > int(tmp_min[0]):
			points = 0
		elif int(tmp_time[0]) == int(tmp_min[0]):
			if int(tmp_time[1]) >= int(tmp_min[1]):
				points = 0

		if points == -1:
			for key, value in point_dict.items():
				for val in value:
					if time == val:
						return key
		return points

	def calculate(self):
		p_total = self.pushups.data
		s_total = self.situps.data
		r_time = self.run.data
		w_total = self.wst.data
		impl_err = 'This age bracket has not been implemented.'

		if self.gender.data == 'male':
			if self.age.data == 'u30':
				# We should probably define minimum/maximum passing values as constants and pass them in that way
				p_points = self.get_str_points(self.mu30_pushup_dict, p_total, 32, 67)
				s_points = self.get_str_points(self.mu30_situp_dict, s_total, 41, 58)
				r_points = self.get_run_points(self.mu30_run_dict, r_time, '13:37', '9:12')
				w_points = self.get_wst_points(self.mu30_waist_dict, w_total, 39.5, 35.0)
				self.get_composite_score(r_points, w_points, p_points, s_points)
			elif self.age.data == 'u40':
				p_points = self.get_str_points(self.mu40_pushup_dict, p_total, 26, 57)
				s_points = self.get_str_points(self.mu40_situp_dict, s_total, 38, 54)
				r_points = self.get_run_points(self.mu40_run_dict, r_time, '14:01', '9:34')
				w_points = self.get_wst_points(self.mu40_waist_dict, w_total, 39.5, 35.0)
				self.get_composite_score(r_points, w_points, p_points, s_points)
			elif self.age.data == 'u50':
				flash(impl_err, u'danger')
			elif self.age.data == 'u60':
				flash(impl_err, u'danger')
			elif self.age.data == 'u70':
				flash(impl_err, u'danger')
		else:  # Must be female
			if self.age.data == 'u30':
				p_points = self.get_str_points(self.fu30_pushup_dict, p_total, 17, 47)
				s_points = self.get_str_points(self.fu30_situp_dict, s_total, 37, 54)
				r_points = self.get_run_points(self.fu30_run_dict, r_time, '16:23', '10:23')
				w_points = self.get_wst_points(self.fu30_waist_dict, w_total, 36.0, 31.5)
				self.get_composite_score(r_points, w_points, p_points, s_points)
			elif self.age.data == 'u40':
				p_points = self.get_str_points(self.fu40_pushup_dict, p_total, 13, 46)
				s_points = self.get_str_points(self.fu40_situp_dict, s_total, 28, 45)
				r_points = self.get_run_points(self.fu40_run_dict, r_time, '16:58', '10:51')
				w_points = self.get_wst_points(self.fu40_waist_dict, w_total, 36.0, 31.5)
				self.get_composite_score(r_points, w_points, p_points, s_points)
			elif self.age.data == 'u50':
				flash(impl_err, u'danger')
			elif self.age.data == 'u60':
				flash(impl_err, u'danger')
			elif self.age.data == 'u70':
				flash(impl_err, u'danger')


@app.route('/pt/', methods=['GET', 'POST'])
def pt():
	form = PTForm(request.form)

	if request.method == 'POST':
		if not form.validate():
			flash(msg_form_error, u'danger')
		else:
			form.calculate()
			for key in form.errors.keys():
				if key == 'calc_error':
					flash(form.errors['calc_error'], u'danger')

	resp = make_response(render_template('pt.html', form=form, info=pt_info))
	resp.headers.set(u'Content-Security-Policy', csp_policy)
	return resp


@app.route('/', methods=['GET'])
def index():
	with open('README.md', 'r') as f:
		readme = f.read()
	readme = Markup(markdown.markdown(readme))
	resp = make_response(render_template('index.html', content=readme, modules=modules))
	resp.headers.set(u'Content-Security-Policy', csp_policy)
	return resp


if __name__ == '__main__':
	app.run()
